﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] obstacles;
    public GameObject enemy, token, pathColor;
    public int tokenSpawnFrequency = 8;
    public float timeBetweenObstacleSpawns, minTimeBetweenSpawns, offsetY = 0;
    int spawnCode = 0;
    //0 = normal, 1 = red, 2 = yellow, 3 = obstacle
    int randomRedLength;
    int randomYellowLength;
    int randomWhiteLength;
    int randomObstacleLength;
    public float timeBetweenEnemies = 1.3f, firstEnemySpawn = 0.5f, firstObstacleSpawn = 1f, aheadOfPlayer = 100f;
    public PathFollow mapBuild;
    bool isStop = false;

    void Start()
    {
        randomRedLength = Random.Range(5, 15);
        randomYellowLength = Random.Range(5, 15);
        randomWhiteLength = Random.Range(10, 20);
        randomObstacleLength = Random.Range(1, 3); ;
        GetComponent<PathFollow>().alpha = 5;
        StartCoroutine(SpawnCr());
    }

    IEnumerator SpawnCr()
    {
        if (GetComponent<PathFollow>().alpha < 95)
        {
            if (spawnCode == 3)
            {
                GameObject tempObstacle = Instantiate(obstacles[Random.Range(0, obstacles.Length)], transform.position, transform.rotation);
                Vector3 obstPos = transform.position;
                obstPos.y += offsetY;
                //tempObstacle.GetComponent<MegaWorldPathDeform>().path
                tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = randomObstacleLength;

                mapBuild.alpha += randomObstacleLength;
                randomWhiteLength = Random.Range(10, 20);
                spawnCode = 0;
                yield return new WaitForSeconds(0.04f);
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
            else if (spawnCode == 2)
            {
                GameObject tempObstacle = Instantiate(pathColor, transform.position, transform.rotation);
                Vector3 obstPos = transform.position;
                obstPos.y += offsetY;
                tempObstacle.GetComponent<MeshRenderer>().material.color = Color.yellow;
                tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = randomYellowLength;

                mapBuild.alpha += randomYellowLength;
                randomObstacleLength = Random.Range(1, 3);
                spawnCode = 3;
                yield return new WaitForSeconds(0.04f);
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
            else if (spawnCode == 1)
            {
                GameObject tempObstacle = Instantiate(pathColor, transform.position, transform.rotation);
                Vector3 obstPos = transform.position;
                obstPos.y += offsetY;
                tempObstacle.GetComponent<MeshRenderer>().material.color = Color.red;
                tempObstacle.GetComponent<MegaWorldPathDeform>().stretch = randomRedLength;

                mapBuild.alpha += randomRedLength;
                randomYellowLength = Random.Range(5, 15);
                spawnCode = 2;
                yield return new WaitForSeconds(0.04f);
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
            else
            {
                mapBuild.alpha += randomWhiteLength;
                randomRedLength = Random.Range(5, 15);
                spawnCode = 1;
                yield return new WaitForSeconds(0.04f);
                if (!FindObjectOfType<CollisionDetect>().gameIsOver)
                    StartCoroutine(SpawnCr());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Finish"))
        {
            isStop = true;
        }
    }
}

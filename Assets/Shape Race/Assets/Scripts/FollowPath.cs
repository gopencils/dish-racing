﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    public Transform targetFollow;
    public PathFollow followPath;

    void Awake()
    {
        //followPath = GetComponent<PathFollow>();
    }

    void Update()
    {
        if(CompareTag("Spawner"))
        {
            followPath.distance = targetFollow.GetComponent<MegaWorldPathDeform>().distance + 10;
        }
        else
            followPath.distance = targetFollow.GetComponent<MegaWorldPathDeform>().distance;
    }
}

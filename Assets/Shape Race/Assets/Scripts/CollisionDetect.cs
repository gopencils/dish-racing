﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetect : MonoBehaviour {

    public Mesh[] meshes;
    public ParticleSystem tokenParticle;

    [HideInInspector]
    public bool gameIsOver = false;

    private MeshFilter playerMesh;
    //private ParticleSystem playerParticle;
    private Animation cameraAnim;


    void Start () {
        //Initizalization
        playerMesh = GetComponent<MeshFilter>();
        //playerParticle = GetComponent<ParticleSystem>();
        cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>();
	}
	
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))      //If Player collided with an enemy
        {
            //playerParticle.Play();      //Plays playerParticle
            other.GetComponent<ParticleSystem>().Play();
            cameraAnim.Play();      //Plays the animation attached to the Main Camera
        }
        else if (other.CompareTag("Obstacle"))         //If Player collided with an obstacle
        {
            //playerParticle.Play();
            other.GetComponent<ParticleSystem>().Play();
            cameraAnim.Play();      //Plays the animation attached to the Main Camera
        }
        //else if (other.CompareTag("Token"))     //If player collides with a token
        //{
        //    Destroy(other.gameObject);      //Destroys token
        //    Destroy(Instantiate(tokenParticle, transform.position, transform.rotation), 1.5f);      //Instantiates tokenParticle, then destroys it after x seconds
        //    FindObjectOfType<ScoreManager>().IncrementToken();      //Increments tokenCounter
        //}
    }

    public void ChangeMesh()
    {
        //Selects a random mesh from the meshes list until it is a different mesh than the player's mesh. Then changes player' s mesh to the selected one
        int index;
        do
        {
            index = Random.Range(0, meshes.Length);
        } while (playerMesh.mesh.name == meshes[index].name + " Instance");
        playerMesh.mesh = meshes[index];
    }
}

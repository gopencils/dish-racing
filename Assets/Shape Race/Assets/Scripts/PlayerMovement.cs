﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;
    private Rigidbody rb;
    public GameObject target;
    float defaultSpeed;
    float a = 0;
    MegaWorldPathDeform worldPathDeform;

    void Start()
    {
        //Initializations\
        worldPathDeform = GetComponent<MegaWorldPathDeform>();
        defaultSpeed = worldPathDeform.speed;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (transform.GetComponent<MegaTwist>().angle > -500)
            {
                transform.GetComponent<MegaTwist>().angle-=10;
            }
            if (a < 720)
            {
                a += 10;
            }
            worldPathDeform.rotate += a*Time.deltaTime;
            if (worldPathDeform.speed < 15)
            {
                worldPathDeform.speed += 10 * Time.deltaTime;
            }
        }
        else
        {
            if (a > 0)
            {
                a -= 10;
            }
            worldPathDeform.rotate += a * Time.deltaTime;
            if (transform.GetComponent<MegaTwist>().angle < 0)
            {
                transform.GetComponent<MegaTwist>().angle+=10;
            }
            //worldPathDeform.rotate = Mathf.MoveTowards(worldPathDeform.rotate, 0, 5*Time.deltaTime);
            worldPathDeform.speed = Mathf.MoveTowards(worldPathDeform.speed, defaultSpeed, 5*Time.deltaTime);
        }
    }
}
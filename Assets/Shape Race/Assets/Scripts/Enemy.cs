﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour {


    public float movementSpeed = 5f;

    private MeshFilter enemyMesh;
    private Vector3 newPos;
    private float posX;
    private bool canMove = true, moveRight = false, moveLeft = false;

    [Header("Snake Manager")]
    public int startCount;
    public Transform prefab;
    private Vector3 lastPosition;
    public List<Transform> balls = new List<Transform>();
    private List<Vector3> points = new List<Vector3>();
    public Transform center;
    Vector3 dirCast;
    int addCount = 0;
    public float size;
    public float height;
    public float standardDistance;
    public GameObject target;
    private Vector3 velocity = Vector3.zero;
    float followSpeed;

    void Start()
    {
        enemyMesh = transform.GetChild(0).GetComponent<MeshFilter>();
        transform.localPosition = Vector3.zero;
        InitSnake();
        followSpeed = 0.05f;
    }

    void FixedUpdate()
    {
        if (moveLeft)
            MoveLeft();
        if (moveRight)
            MoveRight();
    }

    void Update()
    {
        if (balls.Count > 0)
        {
            SnakeMove();
        }
        if (balls.Count > 0)
        {
            try
            {
                Vector3 vector4 = target.transform.position - balls[0].transform.position;
                float magnitude = vector4.magnitude;
                if (magnitude > 0)
                {
                    balls[0].transform.position = Vector3.SmoothDamp(balls[0].transform.position, target.transform.position, ref velocity, 0);
                }
                balls[0].transform.rotation = target.transform.rotation;
            }
            catch { }
        }
        if (transform.childCount < 1)
            Destroy(gameObject);
    }

    void InitSnake()
    {
        //prefab.transform.localScale = Vector3.one * 1.25f;
        for (int i = 0; i < startCount; i++)
        {
            Transform _transform = Instantiate(prefab).transform;
            _transform.parent = transform.parent;
            balls.Add(_transform);
            points.Add(balls[i].position);
            _transform.name = i.ToString();
            _transform.GetComponent<Renderer>().material.color = transform.GetChild(0).GetComponent<Renderer>().material.color;
            //_transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", listColors[currentColor] * 0.1f);
            //colorChangeCount++;
            //if (colorChangeCount >= 5)
            //{
            //    currentColor++;
            //    if (currentColor > listColors.Count - 1)
            //    {
            //        currentColor = 0;
            //    }
            //    colorChangeCount = 0;
            //}
        }
        lastPosition = balls[0].position;
    }

    private void SnakeMove()
    {
        if (lastPosition != balls[0].position)
        {
            lastPosition = balls[0].position;
            points.Insert(0, lastPosition);

            int num = 1;
            float spacing = standardDistance;
            int index = 0;
            while ((index < (this.points.Count - 1)) && (num < this.balls.Count))
            {
                Vector3 vector2 = this.points[index];
                Vector3 vector3 = this.points[index + 1];
                Vector3 vector4 = vector3 - vector2;
                float magnitude = vector4.magnitude;
                if (magnitude > 0f)
                {
                    Vector3 vector6 = vector3 - vector2;
                    Vector3 normalized = vector6.normalized;
                    Vector3 vector7 = vector2;
                    while ((spacing <= magnitude) && (num < this.balls.Count))
                    {
                        vector7 += (Vector3)(normalized * spacing);
                        magnitude -= spacing;
                        this.balls[num].transform.position = vector7;
                        if (num + 1 <= balls.Count - 1)
                            this.balls[num + 1].transform.rotation = this.balls[num].transform.rotation;
                        num++;
                        spacing = standardDistance;
                    }
                    spacing -= magnitude;
                }
                index++;
            }
            Vector3 vector8 = this.points[this.points.Count - 1];
            for (int i = num; i < this.balls.Count; i++)
            {
                balls[num].transform.position = vector8;
                if (num + 1 <= balls.Count - 1)
                    this.balls[num + 1].transform.rotation = this.balls[num].transform.rotation;
            }
            index++;
            if (index < this.points.Count)
            {
                this.points.RemoveRange(index, this.points.Count - index);
            }
        }
    }

    public void RemoveSnake()
    {
        Vector3 zero = Vector3.zero;

        //remove the main ball
        if (this.balls.Count > 0)
        {
            balls[0].gameObject.SetActive(false);
            zero = balls[0].position;
            this.balls.RemoveAt(0);
        }

        //gameover
        if (this.balls.Count <= 0)
        {
            // game over
            GetComponent<CollisionDetect>().gameIsOver = true;      //Game is over
            FindObjectOfType<AudioManager>().DeathSound();      //Plays DeathSound
            FindObjectOfType<GameManager>().EndPanelActivation();       //Activates endPanel
            FindObjectOfType<PlayerMovement>().enabled = false;     //Stops player
            //FindObjectOfType<FollowPath>().enabled = false;     //Stops player
            GetComponent<Collider>().enabled = false;       //Disables collider
            GetComponent<Renderer>().enabled = false;       //Makes player invisible
        }
        else // set next ball as main ball
        {
            Vector3 vector2 = balls[0].position;
            Vector3 vector4 = zero - vector2;
            Vector3 normalized = vector4.normalized;
            int count = 0;
            for (int i = 0; i < this.points.Count; i++)
            {
                if (Vector3.Dot(vector2 - this.points[i], normalized) >= 0f)
                {
                    break;
                }
                count = i;
            }

            points.RemoveRange(0, count);
            SnakeMove();
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if ((other.CompareTag("Obstacle")) && (transform.childCount >= 1))       //If enemy collides with an obstacle
        {
            if (other.GetComponent<MeshFilter>().mesh.name != enemyMesh.mesh.name)      //If ahead of the enemy there is a obstacle with a different mesh
            {
                if (canMove)
                {
                    if ((moveRight == false) && (moveLeft == false))
                    {
                        if (transform.localPosition.x == 0f)
                        {
                            if (Random.Range(0, 2) == 0)
                                moveLeft = true;
                            else
                                moveRight = true;
                        }
                        else if (transform.localPosition.x > 0f)
                            moveLeft = true;
                        else
                            moveRight = true;
                    }
                }
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        canMove = false;
        if (other.CompareTag("Obstacle"))       //If obstacle leaves the trigger
            Invoke("Stop", 0.05f);      //Stops enemy on the X axis
        Invoke("CanMoveAgain", 0.6f);
    }

    public void CanMoveAgain()
    {
        canMove = true;
    }

    public void Stop()
    {
        moveLeft = moveRight = false;
        movementSpeed = 5;
    }

    public void MoveLeft()
    {
        newPos = transform.localPosition;
        newPos.x -= 0.1f;
        transform.localPosition = newPos;
        movementSpeed = 3;
    }

    public void MoveRight()
    {
        newPos = transform.localPosition;
        newPos.x += 0.1f;
        transform.localPosition = newPos;
        movementSpeed = 3;
    }
}
